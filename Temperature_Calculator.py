while(True):
    print('\n Temperature Calculator')
    print("\n 1. Celsius to Fahrenheit \n 2. Fahrenheit to Celsius \n 3. Exit")
    option=int(input("\nEnter your option: "))
    if option==1:
        celsius= float(input("\nEnter the temperature: "))
        fahrenheit= (celsius*1.8) + 32
        print("%0.2f degree celsius ==  %0.2f degree fahrenheit"%( celsius ,fahrenheit ))
        break
    elif option==2:
        fahrenheit= float(input("\nEnter the temperature: "))
        celsius=(fahrenheit-32)*5/9
        print("%0.2f degree Fahrenheit %0.2f degree Celsius "%(fahrenheit,celsius))
        break
    elif option==3:
        break
    else:
        print("You've entered wrong input !! ")
